#### nvim_Nvchad_setting

---

##### a. Install Neovim

Refer [Neovim](https://neovim.io/)

##### b. Install Nvchad

refer [Nvchad](https://nvchad.com/)

##### c. clone the setting to nvim folder

- clone repo && cd into

```bash
git come https://gitlab.com/kylls.ccs/nvim_nvchad_setting.git && cd nvim_nvchad_setting
```

- choose the lang and copy the file into there. [Linux. AlmaLinux 9.0]

```bash
cp Nvchad_rust_setting/custom ~/.config/nvim/lua/custom
```

#### Linux

```bash
git clone https://gitlab.com/kylls.ccs/neovim_frame_setting.git && cd neovim_frame_setting && cp Nvchad_all_setting/custom ~/.config/nvim/lua/custom
```

#### Windows

```bash
git clone https://gitlab.com/kylls.ccs/neovim_frame_setting.git && cd neovim_frame_setting && cp Nvchad_all_setting/custom $HOME\AppData\Local\nvim\lua\custom
```
