#!/bin/bash

# with install rio term and nushell


# update the rust version
rustc --version
    if [ $? -eq 0 ]; then
        echo "rust is installed. and start to update"
        rustup update
    else
        echo "start install......."
        curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    fi

# check rio is install or not
rio --version
    if [ $? -eq 0 ]; then
        echo "rio is installed."
    else
        echo "start install......."
        cargo install rioterm
    fi


# check the nushell  is installed or not

nu --version
    if [ $? -eq 0 ]; then
        echo "nushell is installed."
    else
        echo "start install......."
        cargo install nu
    fi


# install CodeNewRoman Nerd font
sudo mkdir /usr/share/fonts/CodeNewRomanfonts &&
cd ~/Downloads/ &&
curl -fLO https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/CodeNewRoman.zip &&
sudo unzip CodeNewRoman.zip -d /usr/share/fonts/CodeNewRomanfonts &&
sudo fc-cache -fv /usr/share/fonts/CodeNewRomanfonts &&
sudo rm -rf /usr/share/fonts/CodeNewRomanfonts &&
sudo rm -rf ~/Downloads/CodeNewRoman.zip


# install JetBrainsMono Nerd font
sudo mkdir /usr/share/fonts/JetBrainsMonofonts &&
cd ~/Downloads/ &&
curl -fLO https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/JetBrainsMonofonts.zip &&
sudo unzip CodeNewRoman.zip -d /usr/share/fonts/CodeNewRomanfonts &&
sudo fc-cache -fv /usr/share/fonts/CodeNewRomanfonts &&
sudo rm -rf /usr/share/fonts/CodeNewRomanfonts &&
sudo rm -rf ~/Downloads/CodeNewRoman.zip

# install Hack Nerd Fonts
sudo mkdir /usr/share/fonts/Hack &&
cd ~/Downloads/ &&
curl -fLO https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/Hack.zip &&
sudo unzip Hack.zip -d /usr/share/fonts/Hack &&
sudo fc-cache -fv /usr/share/fonts/Hack &&
sudo rm -rf /usr/share/fonts/Hack &&
sudo rm -rf ~/Downloads/Hack


