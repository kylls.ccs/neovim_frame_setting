##### Alacritty installation guide

---

###### install

```bash

cargo install alacritty

```

1. On Windows

```bash
Windows:
Copy the alacritty.yml to path below:
%APPDATA%\Roaming\alacritty\alacritty.yml

```

2. On Linux

```bash
Linux:
Copy the alacritty.yml to path below:
.config/alacritty/alacritty.yml

```
