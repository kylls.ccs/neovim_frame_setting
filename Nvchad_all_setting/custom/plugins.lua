local plugins = {
	{
		"williamboman/mason.nvim",
		opts = {
			ensure_installed = {
				"rust-analyzer",
				"typescript-language-server",
				"eslint-lsp",
				"prettier",
				"emmet-ls",
				"pyright",
				"mypy",
				"ruff",
				"black",
				"stylua",
				"clangd",
				"clang-format",
			},
		},
	},
	{
		"neovim/nvim-lspconfig",
		config = function()
			require("plugins.configs.lspconfig")
			require("custom.configs.lspconfig")
		end,
	},
	-- ========== rust setting ==========
	-- install rust auto save
	{
		"rust-lang/rust.vim",
		ft = "rust",
		init = function()
			vim.g.rustfmt_autosave = 1
		end,
	},
	-- install rust-tools
	{
		"simrat39/rust-tools.nvim",
		ft = "rust",
		dependencies = "neovim/nvim-lspconfig",
		opts = function()
			return require("custom.configs.rust-tools")
		end,
		config = function(_, opts)
			require("rust-tools").setup(opts)
		end,
	},
	-- ========== JS TS setting ==========
	-- {
	--   "mfussenegger/nvim-lint",
	--   event = "VeryLazy",
	--   config = function ()
	--     require("custom.configs.lint")
	--   end
	-- },
	-- {
	--   "mhartington/formatter.nvim",
	--   event = "VeryLazy",
	--   opts = function ()
	--     return require "custom.configs.formatter"
	--   end
	-- }
	{
		"jose-elias-alvarez/null-ls.nvim",
		event = "VeryLazy",
		opts = function()
			return require("custom.configs.null-ls")
		end,
	},
	{
		"https://gitlab.com/HiPhish/rainbow-delimiters.nvim",
		config = function()
			local colors = {
				Red = "#EF6D6D",
				Orange = "#FFA645",
				Yellow = "#EDEF56",
				Green = "#6AEF6F",
				Cyan = "#78E6EF",
				Blue = "#70A4FF",
				Violet = "#BDB2EF",
			}
			require("pynappo.theme").set_rainbow_colors("RainbowDelimiter", colors) -- just a helper function that sets the highlights with the given prefix
			local rainbow_delimiters = require("rainbow-delimiters")

			vim.g.rainbow_delimiters = {
				strategy = {
					[""] = rainbow_delimiters.strategy["global"],
					vim = rainbow_delimiters.strategy["local"],
				},
				query = {
					[""] = "rainbow-delimiters",
				},
				highlight = {
					"RainbowDelimiterRed",
					"RainbowDelimiterYellow",
					"RainbowDelimiterOrange",
					"RainbowDelimiterGreen",
					"RainbowDelimiterBlue",
					"RainbowDelimiterCyan",
					"RainbowDelimiterViolet",
				},
			}
		end,
	},

	{
		"nvim-treesitter/nvim-treesitter",
		opts = {
			ensure_installed = {
				-- defaults
				"vim",
				"lua",
				"python",
				"bash",

				-- web dev
				"html",
				"css",
				"javascript",
				"typescript",
				"tsx",
				"json",
				-- "vue", "svelte",

				-- low level
				"c",
				"rust",
				"zig",
			},
		},
	},
	-- lazy
	{
		"sontungexpt/sttusline",
		dependencies = {
			"nvim-tree/nvim-web-devicons",
		},
		event = { "BufEnter" },
		config = function(_, opts)
			require("sttusline").setup({
				-- statusline_color = "#000000",
				statusline_color = "StatusLine",

				-- | 1 | 2 | 3
				-- recommended: 3
				laststatus = 3,
				disabled = {
					filetypes = {
						-- "NvimTree",
						-- "lazy",
					},
					buftypes = {
						-- "terminal",
					},
				},
				components = {
					"mode",
					"filename",
					"git-branch",
					"git-diff",
					"%=",
					"diagnostics",
					"lsps-formatters",
					"copilot",
					"indent",
					"encoding",
					"pos-cursor",
					"pos-cursor-progress",
				},
			})
		end,
	},
}

return plugins
