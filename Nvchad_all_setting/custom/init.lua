vim.api.nvim_command("set rnu")

-- set if using neovide, can use ctrl + + / - to zoom in out the font size
if vim.g.neovide == true then
	vim.api.nvim_command(":lua vim.g.neovide_scale_factor = 1.0")
	vim.api.nvim_command(":lua vim.o.guifont='CodeNewRoman Nerd Font'")
	vim.api.nvim_command(":lua vim.g.neovide_no_idle = true")
	vim.api.nvim_command(":lua vim.g.neovide_refresh_rate = 30")
	vim.api.nvim_command(":lua vim.g.neovide_remember_window_size = true")
	vim.api.nvim_command(":lua vim.g.neovide_refresh_rate_idle = 5")
	vim.api.nvim_command(":lua vim.g.neovide_cursor_vfx_particle_speed = 5.0")
	vim.api.nvim_set_keymap(
		"n",
		"<C-+>",
		":lua vim.g.neovide_scale_factor = vim.g.neovide_scale_factor + 0.1<CR>",
		{ silent = true }
	)
	vim.api.nvim_set_keymap(
		"n",
		"<C-->",
		":lua vim.g.neovide_scale_factor = vim.g.neovide_scale_factor - 0.1<CR>",
		{ silent = true }
	)
	vim.api.nvim_set_keymap("n", "<C-0>", ":lua vim.g.neovide_scale_factor = 1<CR>", { silent = true })
end
