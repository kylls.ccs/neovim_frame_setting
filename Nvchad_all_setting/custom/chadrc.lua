---@type ChadrcConfig
local M = {}

M.ui = {
	theme = "chadracula",
	statusline = {
		theme = "vscode_colored",
		-- theme = "default",
		separator_style = "arrow",
	},
	tabufline = {
		show_numbers = true,
		enabled = true,
		lazyload = true,
	},
	hl_add = {
		IndentBlanklineChar = {
			fg = "green",
			default = true,
			bold = true,
		},
	},
	hl_override = {
		-- Cursor = {
		--   -- reverse = true,
		--   bg = "teal",
		--   fg = "teal",
		-- },
		CursorLine = {
			-- fg = "purple",
			bg = "line",
		},
		-- CursorColumn = {
		--   bg = "grey"
		-- }
	},
}
M.plugins = "custom.plugins"

return M
