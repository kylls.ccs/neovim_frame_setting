-- set relative line numbers
vim.api.nvim_command("set rnu")

-- enable inlay hints to show types
vim.g.dap_virtual_text = true
