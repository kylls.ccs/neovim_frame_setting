---@type ChadrcConfig 
 local M = {}
 M.ui = {theme = 'ayu_light'}
-- read plugins
 M.plugins = 'custom.plugins'
-- read keymappings
 M.mappings = require("custom.mappings")
 return M
